ZeroBot
-
ZeroBot is an IRC bot with a C# core. It is designed to be extensible in order to perform many as many functions as required.
In order to make it easily extensible, ZeroBot utilises IronPython in order to use Python scripts. This means that in order to add
functionality to ZeroBot, you don't need to stop the program and replace it with the changed code. Instead, you can make Python
scripts and add them on the fly, without needing to take down ZeroBot.

Current Functionality:
- Connections [C# core]
- Chat logging [C# core]
- Say [Python]
- Choose [Python]
- Conversions
	- Temperature conversions (C, F and K) [Python]
		- C <-> F
		- C <-> K
		- F <-> K
	- Base conversions (Binary, Octal, Hex, Dec) [Python]
		- Dec <-> Bin
		- Dec <-> Oct
		- Dec <-> Hex
		- Bin <-> Hex
		- Bin <-> Oct
		
Scripts:

Requirements:
	- Method: Entry(sender, channel, command, message)
		- This method is common to all scripts and is a requirement. If no Entry method exists, the script will never run and an error will occur
		- Param: sender
			- The user who last spoke
		Param: channel
			- The channel from which the input came
		Param: command
			- The first word of an input can potentially be a command. To check for this, do " if command[0] == "%" "
		Param: message
			- The remainder of the input. Can potentially be null if the input consists of only one word.
			
	Scripts need to be placed in the /scripts folder. However, you can use subdirectories to organise, your scripts,
	such as /scripts/conversions