﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;

namespace ZeroBot
{
    class Connections
    {
        public static NetworkStream CreateStream(string server, ushort port)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Console.WriteLine("Socket created");
            IPHostEntry host = Dns.GetHostEntry(server);
            Console.WriteLine("Host found");
            IPEndPoint endPoint = new IPEndPoint(host.AddressList.First(), port);
            Console.WriteLine("Endpoint created");
            socket.Connect(server, port);
            Console.WriteLine("Connected");
            NetworkStream nwStream = new NetworkStream(socket);

            return nwStream;
        }

        public static void Connect(string user, string botName, StreamWriter sWriter, StreamReader sReader)
        {
            sWriter.WriteLine("USER " + user + " * * :" + user);
            Console.WriteLine("Sent USER");
            sWriter.WriteLine("NICK " + botName);
            Console.WriteLine("Sent NICK");
        }
    }
}
