﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ZeroBot
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Declarations
            bool quit = false;
            string botName;
            string password;
            string server;
            ushort port;
            string chanLine;
            string[] channels;
            string user;
            char control;
            string welcome;
            NetworkStream nwStream;
            #endregion

            if (!File.Exists("BotConfig.cfg")) //First time setup
            {
                botName = InputUtils.GetName("Bot");
                password = InputUtils.GetPassword();
                server = InputUtils.GenericGetString("Server");
                port = InputUtils.GetPort();
                chanLine = InputUtils.GetChannels();
                channels = chanLine.Split(',');
                user = InputUtils.GetName("User");
                control = InputUtils.GetControlChar();
                welcome = InputUtils.GenericGetString("Join message (optional)");
                if (string.IsNullOrEmpty(welcome) || string.IsNullOrWhiteSpace(welcome))
                {
                    string[] lines = { botName, password, server, port.ToString(), chanLine, user, control.ToString() };
                    File.WriteAllLines("BotConfig.cfg", lines);
                }
                else
                {
                    string[] lines = { botName, password, server, port.ToString(), chanLine, user, control.ToString(), welcome };
                    File.WriteAllLines("BotConfig.cfg", lines);
                }

            }
            else
            {
                Console.WriteLine("BotConfig.cfg exists.");
            }

            string[] BotInfo = File.ReadAllLines("BotConfig.cfg");
            botName = BotInfo[0];
            password = BotInfo[1];
            server = BotInfo[2];
            port = UInt16.Parse(BotInfo[3]);
            channels = BotInfo[4].Split(',');
            user = BotInfo[5];
            control = BotInfo[6][0];
            if (BotInfo.Length > 7)
            {
                welcome = BotInfo[7];
            }
            else
            {
                welcome = "";
            }

            nwStream = Connections.CreateStream(server, port);
            StreamWriter sWriter = new StreamWriter(nwStream);
            StreamReader sReader = new StreamReader(nwStream);
            sWriter.AutoFlush = true;
            Connections.Connect(user, botName, sWriter, sReader);

            BotIO io = new BotIO(sWriter, sReader, server, channels);
            while (!quit)
            {
                string input = sReader.ReadLine();
                if (input != null)
                {
                    Console.WriteLine(input, Encoding.UTF8);
                    string today = DateTime.Today.ToString("yyyy, MM, dd");
                    Thread t = new Thread(() => io.Break(input));
                    t.Start();
                    if (Directory.Exists("logs/" + botName))
                    {
                        File.AppendAllText("logs/" + botName + "/" + "BotLog " + today + ".log", ((DateTime.Now).ToString() + " " + input + "\n"));
                    }
                    else
                    {
                        Directory.CreateDirectory("logs/" + botName);
                        File.AppendAllText("logs/" + botName + "/" + "BotLog " + today + ".log", ((DateTime.Now).ToString() + " " + input + "\n"));
                    }
                }
            }
        }
    }
}
