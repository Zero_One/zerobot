commands = {"say"}
args = {">help"}

def Entry(sender, channel, command, message):
	if command[0] == "%":
		trimmed = command.replace("%", "", 1)
		if trimmed in commands:
			if trimmed == "say":
				if message in args:
					if message == ">help":
						return "PRIVMSG %s :USAGE: %%say <message>" % (channel)
				else:
					return "PRIVMSG %s :%s" % (channel, message)