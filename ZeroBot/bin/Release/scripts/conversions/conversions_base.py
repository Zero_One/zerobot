def Entry(sender, channel, command, message):
    if command[0] == "%":
        trimmed = command.replace("%", "", 1)
        if trimmed in commands:
            output = commands[trimmed](message)
            return "PRIVMSG %s :%s" % (channel, output)

def DecToBin(decimal):
    try:
        decimal = int(decimal)
    except Exception as e:
        return "Error: %s" % e
    return bin(decimal)

def DecToOct(decimal):
    try:
        decimal = int(decimal)
    except Exception as e:
        return "Error: %s" % e
    return oct(decimal)

def DecToHex(decimal):
    try:
        decimal = int(decimal)
    except Exception as e:
        return "Error: %s" % e
    return hex(decimal)

def HexToDec(hexidecimal):
    try:
        decimal = int(hexidecimal, 16)
    except Exception as e:
        return "Error: %s" % e
    return decimal

def HexToBin(hexidecimal):
    try:
        decimal = int(hexidecimal, 16)
    except Exception as e:
        return "Error: %s" % e
    return bin(decimal)

def BinToHex(binary):
    try:
        decimal = int(binary, 2)
    except Exception as e:
        return "Error: %s" % e
    return hex(decimal)

def BinToDec(binary):
    try:
        decimal = int(binary, 2)
    except Exception as e:
        return "Error: %s" % e
    return decimal

def OctToBin(octal):
    try:
        decimal = int(octal, 8)
    except Exception as e:
        return "Error: %s" % e
    return bin(decimal)

def OctToDec(octal):
    try:
        decimal = int(octal, 8)
    except Exception as e:
        return "Error: %s" % e
    return decimal

commands = {"dec2bin": DecToBin, "dec2oct": DecToOct, "dec2hex": DecToHex, "hex2dec": HexToDec, "hex2bin": HexToBin, "bin2hex": BinToHex, "bin2dec": BinToDec, "oct2bin": OctToBin, "oct2dec": OctToDec}