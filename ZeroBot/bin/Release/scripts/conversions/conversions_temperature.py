def Entry(sender, channel, command, message):
    if command[0] == "%":
        trimmed = command.replace("%", "", 1)
        if trimmed in commands:
            output = commands[trimmed](message)
            return "PRIVMSG %s :%s" % (channel, output)

def C2F(temperature):
    try:
        celsius = float(temperature)
    except Exception as e:
        return "ERROR: %s" % e
    return celsius * 9 / 5 + 32

def C2K(temperature):
    try:
        celsius = float(temperature)
    except Exception as e:
        return "ERROR: %s" % e
    return celsius + 273.15

def F2C(temperature):
    try:
        fahrenheit = float(temperature)
    except Exception as e:
        return "ERROR: %s" % e
    return (fahrenheit - 32) * 5 / 9

def F2K(temperature):
    try:
        fahrenheit = float(temperature)
    except Exception as e:
        return "ERROR: %s" % e
    return F2C(fahrenheit) + 273.15

def K2C(temperature):
    try:
        kelvin = float(temperature)
    except Exception as e:
        return "ERROR: %s" % e
    return kelvin - 273.15

def K2F(temperature):
    try:
        kelvin = float(temperature)
    except Exception as e:
        return "ERROR: %s" % e
    return (kelvin - 273.15) * 9 / 5 + 32

commands = {"c2f": C2F, "c2k": C2K, "f2c": F2C, "f2k": F2K, "k2c": K2C, "k2f": K2F}