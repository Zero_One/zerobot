import sys
sys.path.append("C:\\Program Files (x86)\\IronPython 2.7\\Lib\\")
import random

commands = {"choose"}
args = {">help"}

def Entry(sender, channel, command, message):
	if command[0] == "%":
		trimmed = command.replace("%", "", 1)
		if trimmed in commands:
			if trimmed == "choose":
				if message in args:
					if message == ">help":
						return "PRIVMSG %s :USAGE: %%choose <choice1> | <choice2>" % (channel)
				else:
					output = Choose(message)
					return "PRIVMSG %s :%s" % (channel, output)
				
def Choose(message):
	choices = message.split("|")
	return random.choice(choices).strip()