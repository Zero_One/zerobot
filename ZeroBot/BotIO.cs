﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace ZeroBot
{
    class BotIO
    {
        private string server;
        private List<string> channelList = new List<string>();
        private StreamWriter sWriter;
        private StreamReader sReader;
        public Queue<string> sendQueue = new Queue<string>();
        private ScriptRuntime ipy;
        private dynamic pyScript;
        private string output;

        public BotIO(StreamWriter sWriter, StreamReader sReader, string server, string[] channels)
        {
            string[] serverArray = server.Split(new[] { '.' }, 2);
            this.server = serverArray[1];
            foreach (string channel in channels)
            {
                channelList.Add(channel);
            }
            this.sWriter = sWriter;
            this.sReader = sReader;
            Thread sendThread = new Thread(Send);
            sendThread.Start();
        }

        public void DoScripts(string[] parts)
        {
            string host = parts[0];
            string[] hostParts = host.Split('!');
            if (hostParts.Length < 2)
            {
                return;
            }
            string sender = hostParts[0].Remove(0, 1);
            string hostMask = hostParts[1];
            string messageType = parts[1];
            string channel = parts[2];
            string message = parts[3].Remove(0, 1);
            string[] messageParts = message.Split(new[] { ' ' }, 2);
            string command, finalMessage;
            if (messageParts.Length > 1)
            {
                command = messageParts[0];
                finalMessage = messageParts[1];
            }
            else
            {
                command = "";
                finalMessage = message;
            }
            foreach (string script in Directory.GetFiles(@"scripts\", "*.py", SearchOption.AllDirectories))
            {
                try
                {
                    ipy = Python.CreateRuntime();
                    pyScript = ipy.UseFile(script);
                    output = pyScript.Entry(sender, channel, command, finalMessage);
                    if (output != null)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine(output);
                        Console.ForegroundColor = ConsoleColor.White;
                        sendQueue.Enqueue(output);
                    }
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e);
                    Console.ResetColor();
                }
            }
        }

        public void Break(string input)
        {
            string[] parts = input.Split(new[] {' '}, 4);
            if (parts.Count() < 4 || parts[0].Contains(server))
            {
                CheckServMsg(parts);
            }
            else
            {
                DoScripts(parts);
            }
        }

        public void CheckServMsg(string[] input)
        {
            if (input[0].Contains(server))
            {
                if (input[1] == "376")
                {
                    foreach (string channel in channelList)
                    {
                        sendQueue.Enqueue(MessageGen.Join(channel));
                    }
                }
            }
            else if (input[0] == "PING")
            {
                sendQueue.Enqueue(MessageGen.Pong(input[1]));
            }
        }
        
        private void Send()
        {
            while (true)
            {
                if (sendQueue.Count > 0)
                {
                    string message;
                    if ((message = sendQueue.Dequeue().ToString()) != null)
                    //if (message != null)
                    {
                        sWriter.WriteLine(message);
                    }
                    Thread.Sleep(25);
                }
            }
        }
    }
}
