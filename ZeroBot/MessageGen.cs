﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZeroBot
{
    class MessageGen // Entire class likely to be moved into individual Python scripts at a later time
    {
        public static string Pong(string timestamp)
        {
            return string.Format("PONG {0}", timestamp);
        }

        public static string Join(string channel)
        {
            Console.WriteLine("Writing JOIN statement");
            return string.Format("JOIN {0}", channel);
        }

        public static string Part(string channel)
        {
            return string.Format("PART {0}", channel);
        }

        public static string Quit(string quitMessage)
        {
            return string.Format("QUIT {0}", quitMessage);
        }

        public static string PrivMsg(string channel, string message)
        {
            return string.Format("PRIVMSG {0} :{1}", channel, message);
        }
    }
}
