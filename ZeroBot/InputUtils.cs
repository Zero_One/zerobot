﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ZeroBot
{
    class InputUtils
    {
        /// <summary>
        /// Asks the User to input a name and then checks for illegal characters
        /// </summary>
        /// <param name="Name">Bot name or User name</param>
        /// <returns>string</returns>
        public static string GetName(string Name)
        {
            string name;
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("{0} Nickname: ", Name);
                Console.ForegroundColor = ConsoleColor.White;
                name = Console.ReadLine();

                Regex r = new Regex("[^a-zA-Z0-9_\\]\\[-{}^`|\\\\]");
                if (!string.IsNullOrWhiteSpace(name) && !Char.IsDigit(name, 0) && !r.IsMatch(name))
                {
                    return name;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid nickname!");
                    Console.WriteLine(@"Nick can only contain characters a-z A-Z 0-9 _ - \ [ ] { } ^ ` |");
                    Console.WriteLine("Characters 0-9 and '-' may not be in the first position.");
                }
            }
        }

        /// <summary>
        /// Prompts the user for their Nickserv password
        /// </summary>
        /// <returns>string</returns>
        public static string GetPassword()
        {
            string pass;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Nickserv Password (leave blank if you don't have one): ");
            Console.ForegroundColor = ConsoleColor.White;
            // TODO: Add in password masking
            pass = Console.ReadLine();
            return pass;
        }

        /// <summary>
        /// Prompts the user for string information
        /// </summary>
        /// <param name="data">The name of the data I.E., server or welcome message</param>
        /// <returns></returns>
        public static string GenericGetString(string data)
        {
            string info;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0}: ", data);
            Console.ForegroundColor = ConsoleColor.White;
            info = Console.ReadLine();
            return info;
        }

        /// <summary>
        /// Ensures that the given port is a ushort
        /// </summary>
        /// <returns>ushort</returns>
        public static ushort GetPort()
        {
            string port;
            ushort parsedPort;

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Port (usually 6667): ");
                Console.ForegroundColor = ConsoleColor.White;
                port = Console.ReadLine();
                if (UInt16.TryParse(port, out parsedPort))
                {
                    return parsedPort;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid port number! Range: 0 - 65535");
                }
            }
        }

        /// <summary>
        /// Prompt the user for a list of channels and passwords and check the channels are valid
        /// in that they can be connected to, not that they exist currently
        /// </summary>
        /// <returns>string</returns>
        public static string GetChannels()
        {
            string input;
            string[] channels;
            bool success;

            while (true)
            {
                success = true;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Channels (Format: #channel1 password1,&channel2 password2, ...): ");
                Console.ForegroundColor = ConsoleColor.White;
                input = Console.ReadLine();
                channels = input.Split(',');

                foreach (string channel in channels)
                {
                    if (success == true && (channel[0] == '#' || channel[0] == '&'))
                    {

                    }
                    else
                    {
                        success = false;
                    }
                }

                if (success == true)
                {
                    return input;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Bad channel detected. Make sure the channel name starts with a '#' or a '&'");
                }
            }
        }

        /// <summary>
        /// Prompts the user for the control character for the bot
        /// </summary>
        /// <returns>char</returns>
        public static char GetControlChar()
        {
            string input;
            char control;

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Control Key: ");
                Console.ForegroundColor = ConsoleColor.White;
                input = Console.ReadLine();

                if (input.Length == 1)
                {
                    control = Convert.ToChar(input);
                    return control;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Key can only be a single character!");
                }
            }
        }
    }
}
